declare module 'watson-developer-cloud' {
    namespace watson {
        interface RequestConfig {
            version: string;
            url: string;
            username: string;
            password: string;
        }

        interface ResponseFunction {
            (error: string, token: string): void;
        }

        interface WatsonAuthorizationService {
            getToken(params: { url: string }, response: ResponseFunction): void;
        }

        function authorization(config: RequestConfig): WatsonAuthorizationService;
    }

    export = watson;
}