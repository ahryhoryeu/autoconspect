﻿require.config({
    baseUrl: '/content/app',
    paths: {
        'text': '../bower_components/text/text',
        'global': '../bower_components/require-global/sources/global',

        'jquery': '../bower_components/jquery/dist/jquery',
        'jquery-ui': '../bower_components/jquery-ui/jquery-ui.min',

        'knockout': '../bower_components/knockout/dist/knockout.debug',
        'knockout-sortable': '../bower_components/knockout-sortable/build/knockout-sortable',

        'bootstrap-material': '../bower_components/bootstrap-material-design/dist/js/material',
        'material-ripples': '../bower_components/bootstrap-material-design/dist/js/ripples',
        'bootstrap-modal': '../bower_components/bootstrap/js/modal',
        'moment': '../bower_components/moment/moment',
        'spin': '../bower_components/spin.js/spin',
        'jquery.blockUI': '../bower_components/blockui/jquery.blockUI',
        'chartjs': '../bower_components/chart.js/dist/Chart',
        'watson-speech': '../bower_components/speech-javascript-sdk/dist/watson-speech'
    },
    shim: {
        'jquery-ui': ['jquery'],
        'jquery.blockUI': ['jquery'],
        'knockout-sortable': ['knockout', 'jquery-ui'],
        'bootstrap-material': ['jquery'],
        'material-ripples': ['jquery'],
        'bootstrap-modal': ['jquery'],
        'chartjs': ['moment']
    },
    map: {
        '*': {
            'jquery-ui/sortable': 'jquery-ui',
            'jquery-ui/draggable': 'jquery-ui',
            'jquery.ui': 'jquery-ui'
        }
    }
});