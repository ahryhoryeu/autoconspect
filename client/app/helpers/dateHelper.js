define(function (require) {
    var moment = require('moment');
    var dateFormat = require('constants/dateFormat');

    var parseDateTimeString = function (value, format) {
        return moment(value, format || dateFormat.serverDateTime);
    };

    var parseDateString = function (value, format) {
        return moment.utc(value, format || dateFormat.serverDate);
    };

    var getDateTimeString = function (value, format) {
        return moment(value).format(format || dateFormat.serverDateTime);
    };

    var getDateString = function (value, format) {
        return moment.utc(value).format(format || dateFormat.serverDate);
    };

    return {
        parseDateTimeString: parseDateTimeString,
        parseDateString: parseDateString,
        getDateTimeString: getDateTimeString,
        getDateString: getDateString
    };
});