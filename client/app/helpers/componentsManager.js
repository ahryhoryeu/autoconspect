define(function (require) {
    'use strict';

    var ko = require('knockout');

    function registerComponent(namespace, component) {
        ko.components.register(component, { require: namespace + '/' + component + '/' + component });

    }

    function registerNamespace(namespace, components) {
        components.forEach(component => registerComponent(namespace, component));
    }

    function register(components) {
        Reflect.ownKeys(components).forEach(namespace => registerNamespace(namespace, components[namespace]));
    }

    return {
        register: register,
        registerComponent: registerComponent,
        registerNamespace: registerNamespace
    };
});