define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Spinner = require('spin');
    require('jquery.blockUI');


    function show(element, options) {
        function getSpinner(opt) {
            var spinnerDefaults = {
                lines: 13, // The number of lines to draw
                length: 20, // The length of each line
                width: 10, // The line thickness
                radius: 30, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#FFF', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
            };
            var $target = $("<div class='spinner-wrapper'></div>").css({
                position: "absolute",
                top: "50%",
                left: "50%"
            });

            var spinnerOptions = $.extend({}, spinnerDefaults, opt);

            new Spinner(spinnerOptions).spin($target[0]);

            return $target;
        }

        //TODO: move styles to css or use other plugin
        var layoutOptions = {
            css: {
                background: "none",
                border: "none",
                width: "100%",
                height: "100%",
                top: 0,
                left: 0
            },
            baseZ: 2000
        };


        if (element) {
            layoutOptions.message = getSpinner(options);
            $(element).block(layoutOptions);
        } else {
            layoutOptions.message = getSpinner();
            $.blockUI(layoutOptions);
        }
    }

    function hide(element, options) {
        if (element) {
            $(element).unblock(options);
        } else {
            $.unblockUI(options);
        }
    }

    return {
        show: show,
        hide: hide
    };
});
