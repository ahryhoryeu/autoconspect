﻿define(function (require) {
    'use strict';

    var $ = require('jquery');

    var requestConfig = {
        contentType: 'application/json',
        cache: false
    };

    function extendRequestConfig(config) {
        requestConfig = $.extend(requestConfig, config);
    }

    function getRequestConfig() {
        return $.extend({}, requestConfig);
    }

    function setUp(config) {
        $.extend($.ajaxSettings, config);
        return this;
    }

    return {
        extendRequestConfig: extendRequestConfig,
        getRequestConfig: getRequestConfig,
        setUp: setUp
    };

});