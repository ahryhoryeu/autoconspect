define(function (require) {
    'use strict';

    let moment = require('moment');
    let Chart = require('chartjs');

    let defaultOptions = {
        type: 'line',
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {beginAtZero: true}
                    }
                ]
            }
        }
    };

    class LineChart {
        constructor(element, labels, ...dataSets) {
            let data = {
                labels: labels,
                datasets: dataSets
            };
            let options = Object.assign({}, defaultOptions);
            options.data = data;

            this.chart = new Chart(element, options);
        }
    }

    return LineChart;
});