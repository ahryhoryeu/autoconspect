define(function (require) {
    'use strict';

    var $ = require('jquery');

    function getData(uri) {
        var rawContent = $('#' + uri).html();
        var encodedContent = $('<div>').html(rawContent).text();
        var data = null;
        if (encodedContent) {
            data = JSON.parse(encodedContent);
        }

        return new Promise(resolve => resolve(data));
    }

    return {
        getData: getData
    };
});
