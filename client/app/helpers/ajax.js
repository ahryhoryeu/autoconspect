﻿define(function (require) {
    'use strict';

    var $ = require('jquery');
    var ko = require('knockout');
    var ajaxConfigurator = require('helpers/ajaxConfigurator');

    function sendAjax(defaultConfig, config) {

        var commonConfig = ajaxConfigurator.getRequestConfig();
        var options = $.extend({}, commonConfig, defaultConfig);
        options = $.extend(options, config);

        if (defaultConfig.data) {
            options.data = JSON.stringify(options.data);
        }

        return $.ajax(options);
    }

    function get(url, config) {
        var defaultConfig = {
            url: url,
            type: 'GET'
        };

        return sendAjax(defaultConfig, config);
    }

    function post(url, data, config) {
        var defaultConfig = {
            url: url,
            type: 'POST',
            data: data
        };

        return sendAjax(defaultConfig, config);
    }

    function put(url, data, config) {
        var defaultConfig = {
            url: url,
            type: 'PUT',
            data: data
        };

        return sendAjax(defaultConfig, config);
    }

    function remove(url, config) {
        var defaultConfig = {
            url: url,
            type: 'DELETE'
        };

        return sendAjax(defaultConfig, config);
    }

    return {
        'get': get,
        'post': post,
        'put': put,
        'delete': remove
    };
});