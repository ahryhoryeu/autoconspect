define(function (require) {
    'use strict';

    var $ = require('jquery');
    var document = require('global!window.document');

    //Fix for dynamic templates in ie9
    function add(name, template) {
        if (!document.getElementById(name)) {
            $('<script type="text/html">' + template + '<\/script>').attr({id: name}).appendTo('body');
        }
    }

    return {
        add: add
    };
});
