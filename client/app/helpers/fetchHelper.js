define(function (require) {
    'use strict';

    function status(response) {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response)
        } else {
            return Promise.reject(new Error(response.statusText))
        }
    }

    function json(response) {
        return response.json();
    }

    function sendAjax(request) {
        return fetch(request)
            .then(status)
            .then(json);
    }

    function get(url, params) {
        if (params && params.data) {
            var data = Object.getOwnPropertyNames(params.data)
                .forEach(name => `${name.toString()}=${params.data[name].toString()}`);
            url = `${url}?${data.join('&')}`;
        }

        var request = new Request(url, {
            method: 'GET',
            credentials: 'include'
        });

        return sendAjax(request);
    }

    function post(url, data) {
        var request = new Request(url, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json"
            }
        });

        return sendAjax(request);
    }

    return {
        get: get,
        post: post
    }
});