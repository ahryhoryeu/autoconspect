define(function (require) {
    'use strict';

    var $ = require('jquery');

    return {
        isFunction: $.isFunction,
        isArray: $.isArray,
        type: $.type
    };
});
