define(function (require) {
    'use strict';

    let template = require('text!components/fieldsets-form/fieldsets-form.html');
    let formActionTypes = require('constants/formActionTypes');

    class FormViewModel {
        constructor(params) {
            this.ViewModel = params.ViewModel;
            this.ViewModelArguments = params.ViewModelArguments;

            if (!params.viewModel) {
                this.viewModel = !this.ViewModelArguments
                    ? new this.ViewModel()
                    : new this.ViewModel(...this.ViewModelArguments);
            } else {
                this.viewModel = params.viewModel.clone();
            }

            this.actions = params.actions.map(action => ({
                css: getButtonCss(action.type),
                text: action.text,
                onAction: this.wrapCallback(action.onAction)
            }));

            this.fieldsets = params.fieldsets.map(fieldset => {
                var newParams = fieldset.params || {};
                newParams.viewModel = this.viewModel;

                return {
                    name: fieldset.name,
                    params: newParams
                }
            });
        }

        wrapCallback(callback) {
            return () => callback(this.viewModel);
        }
    }

    function getButtonCss(type) {
        switch (type) {
            case formActionTypes.confirm:
                return {'btn-primary': true};
            case formActionTypes.cancel:
                return {'btn-default': true};
            case formActionTypes.remove:
                return {'btn-danger': true};
        }
    }

    return {
        template: template,
        viewModel: FormViewModel
    };
});