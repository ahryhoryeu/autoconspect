define(function(require) {
    'use strict';

    const template = require('text!components/fieldsets/conspect-fieldset/conspect-fieldset.html');

    return { template: template };
});