define(function (require) {
    'use strict';

    const template = require('text!components/conspect-item/conspect-item.html');
    const location = require('global!window.location');
    
    function ConspectItemViewModel(params) {
        this.viewModel = params.viewModel;
        this.recordUrl = `/record/index?conspectId=${this.viewModel.id}`;
        this.onDeleteConspect = params.onDeleteConspect;
    }

    ConspectItemViewModel.prototype.deleteConspect = function() {
        this.onDeleteConspect(this.viewModel);
    };

    ConspectItemViewModel.prototype.goToRecord = function() {
        location.href = this.recordUrl;
    };
    
    return {
        template: template,
        viewModel: ConspectItemViewModel
    };
});
