define(function (require) {
    'use strict';

    let ko = require('knockout');
    let LineChart = require('helpers/lineChart');

    ko.bindingHandlers.lineChart = {
        init: function(element, valueAccessor) {
            let opts = valueAccessor();

            let chart = new LineChart(element, ko.unwrap(opts.labels), ...opts.dataSets);
        },
        update: function (element, valueAccessor, allBindingsAccessor) {

        }
    };
});
