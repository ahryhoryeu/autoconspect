define(function (require) {
    'use strict';

    let $ = require('jquery');
    let ko = require('knockout');
    let typeDetector = require('helpers/typeDetector');

    ko.bindingHandlers.subTreeMutationObserver = {
        init: function(element, valueAccessor) {
            let defaultCallback = mutations => {
                $.material.init();
                $.material.ripples();
                $.material.input();

                console.log('Mutation handled');
            };
            let optionCallback = typeDetector.isFunction(valueAccessor())
                ? valueAccessor()
                : null;

            let callback = !!optionCallback
                ? () => {
                    defaultCallback.apply(null, arguments);
                    optionCallback.apply(null, arguments);
                }
                : defaultCallback;

            let observer = new MutationObserver(callback);
            let observerConfig = {
                childList: true,
                attributes: false,
                characterData: false,
                subtree: true
            };

            observer.observe(element, observerConfig);
        }
    }
});