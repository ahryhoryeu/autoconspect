﻿define(function (require) {
    'use strict';

    var ko = require('knockout');
    var spinner = require('helpers/spinner');
    var $ = require('jquery');

    function toggleSpinner(element, params) {
        var $element = $(element);
        var isSpinner = ko.unwrap(params.isSpinner);
        isSpinner ? spinner.show(element, params.options) : spinner.hide(element, params.options);
        $element.toggleClass('spinner-container', !!isSpinner);
    }

    ko.bindingHandlers.spinner = {
        update: function (element, valueAccessor) {
            var params = valueAccessor();
            params.isFullScreen ? toggleSpinner(null, params) : toggleSpinner(element, params);
        }
    };
});