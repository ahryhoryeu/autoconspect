define(function (require) {
    'use strict';

    let ko = require('knockout');
    let dateHelper = require('helpers/dateHelper');
    let dateFormat = require('constants/dateFormat');

    ko.bindingHandlers.date = {
        update: function (element, valueAccessor, allBindingsAccessor) {
            var value = valueAccessor();
            var allBindings = allBindingsAccessor();
            var valueUnwrapped = ko.unwrap(value);

            var pattern = allBindings.format || dateFormat.clientDate;

            var output = "-";
            if (valueUnwrapped !== null && valueUnwrapped !== undefined && valueUnwrapped.length > 0) {
                output = dateHelper.parseDateString(valueUnwrapped).format(pattern);
            }
            element.innerHTML = output;
        }
    };
});