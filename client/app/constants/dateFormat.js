define({
    clientDate: 'DD-MM-YYYY',
    clientDateAlternative: 'DD/MM/YY',
    clientDateTime: 'DD-MM-YYYY HH:mm:ss',
    clientDateTimeAlternative: 'MMMM D, YYYY, HH:mm:ss',
    clientTime: 'h:mm:ss a',

    serverDateTime: 'YYYY-MM-DDTHH:mm:ssZ',
    serverDate: 'YYYY-MM-DDT00:00:00[Z]',

    year: 'YYYY',
    month: 'MMMM YYYY'
});