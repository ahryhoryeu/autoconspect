define({
    watsonToken: {
        querySpeech2Text: '/watson/token/speech2text',
        queryText2Speech: '/watson/token/text2speech'
    },
    conspects: {
        query: '/conspects/all',
        add: '/conspects/add',
        remove: '/conspects/remove',
        edit: '/conspects/edit'
    }
});