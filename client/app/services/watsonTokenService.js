define(function(require) {
    'use strict';

    const ajax = require('helpers/ajax');
    const serviceUrls = require('constants/serviceUrls');

    function querySpeech2Text() {
        return ajax.get(serviceUrls.watsonToken.querySpeech2Text);
    }

    function queryText2Speech() {
        return ajax.get(serviceUrls.watsonToken.queryText2Speech);
    }

    return {
        querySpeech2Text: querySpeech2Text,
        queryText2Speech: queryText2Speech
    };
});