define(function(require) {
    'use strict';

    const ajax = require('helpers/ajax');
    const serviceUrls = require('constants/serviceUrls');

    function query() {
        return ajax.get(serviceUrls.conspects.query);
    }

    function add(viewModel) {
        return ajax.post(serviceUrls.conspects.add, viewModel.toServerDataModel());
    }

    function edit(viewModel) {
        return ajax.post(serviceUrls.conspects.edit, viewModel.toServerDataModel());
    }

    function remove(viewModel) {
        return ajax.post(serviceUrls.conspects.remove, {
            id: viewModel.id
        });
    }

    return {
        query: query,
        add: add,
        edit: edit,
        remove: remove
    };
});