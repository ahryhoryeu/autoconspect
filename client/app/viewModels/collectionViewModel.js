define(function (require) {
    'use strict';

    var ko = require('knockout');
    var typeDetector = require('helpers/typeDetector');

    class CollectionViewModel {
        constructor(collectionModel, ViewModel, ...args) {
            var self = this;
            self.$arguments = args;
            self.ViewModel = ViewModel;

            let items = collectionModel && collectionModel.map && collectionModel.map(item =>
                    new self.ViewModel(...[item].concat(self.$arguments)));

            self.items = ko.observableArray(items);
        }

        fromDataModel(collectionModel) {
            var self = this;
            var viewModelAdditionalParamsList = self.$arguments;

            self.items(collectionModel.map(item =>
                new self.ViewModel(...[item].concat(viewModelAdditionalParamsList))));
        }

        fromViewModel(collectionViewModel) {
            this.fromDataModel(collectionViewModel.toDataModel());
        }

        toDataModel() {
            return getDataModel(this.items, 'toDataModel');
        }

        toServerDataModel() {
            return getDataModel(this.items, 'toServerDataModel');
        }

        clone() {
            var dataModel = this.toDataModel();

            return new this.constructor(...[dataModel, this.ViewModel].concat(this.$arguments));
        }
    }

    function getDataModel(items, methodName) {
        return ko.utils.arrayMap(ko.unwrap(items), function (item) {
            if (typeDetector.type(item) === 'object') {
                if (item[methodName]) {
                    return item[methodName]();
                } else {
                    throw new Error('You cannot convert this viewModel to dataModel. All nested objects should be inherited from the BaseViewModel');
                }
            } else {
                return item;
            }
        });
    }

    return CollectionViewModel;
});