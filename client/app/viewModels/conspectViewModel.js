define(function(require) {
    'use strict';

    const ko = require('knockout');
    const BaseViewModel = require('viewModels/baseViewModel');
    const CollectionViewModel = require('viewModels/collectionViewModel');
    const ConspectPieceViewModel = require('viewModels/conspectPieceViewModel');

    class ConspectViewModel extends BaseViewModel {
        constructor(dataModel) {
            super();
            dataModel = dataModel || {};

            this.id = dataModel.id;
            this.name = ko.observable(dataModel.name);
            this.pieces = new CollectionViewModel(dataModel.pieces, ConspectPieceViewModel);
        }
    }

    return ConspectViewModel;
});