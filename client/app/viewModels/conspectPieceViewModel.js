define(function(require) {
    'use strict';

    const ko = require('knockout');
    const BaseViewModel = require('viewModels/baseViewModel');

    class ConcpectPieceViewModel extends BaseViewModel {
        constructor(dataModel) {
            super();
            dataModel = dataModel || {};

            this.date = ko.observable(dataModel.date);
            this.content = ko.observable(dataModel.content);
        }
    }

    return ConcpectPieceViewModel;
});