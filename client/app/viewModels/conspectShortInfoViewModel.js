define(function(require) {
    'use strict';

    const ko = require('knockout');
    const BaseViewModel = require('viewModels/baseViewModel');

    class ConspectShortInfoViewModel extends BaseViewModel {
        constructor(dataModel) {
            super();
            dataModel = dataModel || {};

            this.id = dataModel.id;
            this.name = ko.observable(dataModel.name);
        }
    }

    return ConspectShortInfoViewModel;
});