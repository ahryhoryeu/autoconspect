define(function (require) {
    'use strict';

    var ko = require('knockout');
    var typeDetector = require('helpers/typeDetector');

    class BaseViewModel {
        constructor(...args) {
            var self = this;

            self.$arguments = args;
        }

        fromDataModel(dataModel) {
            var viewModel = this;
            dataModel = dataModel || {};

            updateViewModel(viewModel, dataModel);
        }

        toDataModel() {
            return getDataModel(this, 'toDataModel');
        }

        fromViewModel(viewModel) {
            var dataModel = null;
            if (viewModel) {
                dataModel = viewModel.toDataModel();
            }
            this.fromDataModel(dataModel);
        }

        clone() {
            var dataModel = this.toDataModel();

            return new this.constructor(...[dataModel].concat(this.$arguments));
        }

        toServerDataModel() {
            return getDataModel(this, 'toServerDataModel');
        }
    }

    var isBaseViewModelOption = BaseViewModel.prototype.isBaseViewModelOption = function (option) {
        var baseViewModelOptions = ['$arguments'];

        return !!ko.utils.arrayFirst(baseViewModelOptions, function (baseViewModelOption) {
            return baseViewModelOption === option;
        });
    };

    function updatePrimitive(viewModel, dataModel, option) {
        viewModel[option] = dataModel[option];
    }

    function updateObservablePrimitive(viewModel, dataModel, option) {
        viewModel[option](dataModel[option]);
    }

    function updateObject(viewModel, dataModel) {
        if (viewModel.fromDataModel) {
            viewModel.fromDataModel(dataModel);
        } else {
            throw new Error('You cannnot update this viewModel from dataModel. All nested objects should be inherited from the BaseViewModel');
        }
    }

    function updateProperty(viewModel, dataModel, option) {
        var viewModelProperty = viewModel[option];
        var dataModelProperty = dataModel[option];

        if (viewModelProperty && viewModelProperty.cloningStrategy) {
            viewModelProperty.cloningStrategy.fromDataModel(dataModelProperty);
        } else {
            var unwrappedViewModelProperty = ko.unwrap(viewModelProperty);

            if (typeDetector.type(unwrappedViewModelProperty) === 'object') {
                updateObject(unwrappedViewModelProperty, dataModelProperty);
            } else if (ko.isWriteableObservable(viewModelProperty)) {
                updateObservablePrimitive(viewModel, dataModel, option);
            } else if (!typeDetector.isFunction(viewModelProperty)) {
                updatePrimitive(viewModel, dataModel, option);
            }
        }
    }

    function updateViewModel(viewModel, dataModel) {
        for (var option in viewModel) {
            if (viewModel.hasOwnProperty(option) && !isBaseViewModelOption(option) && !ko.isComputed(viewModel[option])) {
                updateProperty(viewModel, dataModel, option);
            }
        }
    }

    function copyObject(dataModel, option, viewModelValue, methodName) {
        if (viewModelValue[methodName]) {
            dataModel[option] = viewModelValue[methodName]();
        } else {
            throw new Error('You cannot convert this viewModel to dataModel. All nested objects should be inherited from the BaseViewModel');
        }
    }

    function copyArray(dataModel, option, viewModelValue) {
        dataModel[option] = [];
        ko.utils.arrayForEach(viewModelValue, function (item, index) {
            if (typeDetector.type(item) === "boolean" || typeDetector.type(item) === "number" || typeDetector.type(item) === "string") {
                dataModel[option][index] = item;
            } else {
                dataModel[option] = null;
                throw new Error('You cannot convert this viewModel to dataModel. Arrays can contain just primitive types. Use CollectionViewModel for this list.');
            }
        });
    }

    function copyValue(dataModel, option, viewModelValue, methodName) {
        if (typeDetector.type(viewModelValue) === 'object') {
            copyObject(dataModel, option, viewModelValue, methodName);
        } else if (typeDetector.isArray(viewModelValue)) {
            copyArray(dataModel, option, viewModelValue);
        } else {
            dataModel[option] = viewModelValue;
        }
    }

    function copyProperty(viewModel, dataModel, option, methodName) {
        if (viewModel[option] && viewModel[option].cloningStrategy) {
            dataModel[option] = viewModel[option].cloningStrategy.toDataModel();
        } else if (!ko.isComputed(viewModel[option])) {
            var viewModelValue = ko.unwrap(viewModel[option]);
            copyValue(dataModel, option, viewModelValue, methodName);
        }
    }

    function getDataModel(viewModel, methodName) {
        var dataModel = {};
        for (var option in viewModel) {
            if (viewModel.hasOwnProperty(option) && !isBaseViewModelOption(option)) {
                copyProperty(viewModel, dataModel, option, methodName);
            }
        }
        return dataModel;
    }

    return BaseViewModel;
});