define(function(require) {
    'use strict';

    const ko = require('knockout');
    const CollectionViewModel = require('viewModels/collectionViewModel');
    const ConspectShortInfoViewModel = require('viewModels/conspectShortInfoViewModel');
    const conspectsService = require('services/conspectsService');
    const formActionTypes = require('constants/formActionTypes');
    
    class ConspectsPageViewModel {
        constructor(setupData) {
            this.conspects = new CollectionViewModel(setupData, ConspectShortInfoViewModel);
            this.hasItems = ko.pureComputed(() => this.conspects.items().length > 0);

            this.isAddFormVisible = ko.observable(false);
        }

        toggleFormVisibility() {
            this.isAddFormVisible(!this.isAddFormVisible());
        }

        getConspectUrl(conspect) {
            return `/record/index?id=${conspect.id}`;
        }

        getAddFormParams() {
            var self = this;
            return {
                ViewModel: ConspectShortInfoViewModel,
                actions: [
                    {
                        text: 'Add',
                        type: formActionTypes.confirm,
                        onAction: self.add.bind(self)
                    },
                    {
                        text: 'Cancel',
                        type: formActionTypes.cancel,
                        onAction: () => self.toggleFormVisibility()
                    }
                ],
                fieldsets: [
                    {
                        name: 'conspect-fieldset'
                    }
                ]
            };
        }

        add(viewModel) {
            conspectsService.add(viewModel)
                .then(() => this.toggleFormVisibility())
                .then(() => conspectsService.query())
                .then(data => this.conspects.fromDataModel(data));
        }

        remove(viewModel) {
            conspectsService.remove(viewModel)
                .then(() => conspectsService.query())
                .then(data => this.conspects.fromDataModel(data));
        }
    }

    return ConspectsPageViewModel;
});