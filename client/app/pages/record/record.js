define(function(require) {
    'use strict';

    const ko = require('knockout');
    const WatsonSpeech = require('watson-speech');
    const watsonTokenService = require('services/watsonTokenService');
    const ConspectViewModel = require('viewModels/conspectViewModel');
    const ConspectPieceViewModel = require('viewModels/conspectPieceViewModel');
    const conspectsService = require('services/conspectsService');
    const location = require('global!window.location');
    
    class RecordPageViewModel {
        constructor(setupData) {
            this.token = ko.observable();
            this.stream = ko.observable();
            this.isRecording = ko.observable(false);

            this.conspect = new ConspectViewModel(setupData);
            this.newPiece = new ConspectPieceViewModel();
            this.hasItems = ko.pureComputed(() => this.conspect.pieces.items().length > 0);
        }

        startListening() {
            watsonTokenService.querySpeech2Text()
                .then(token => {
                    this.token(token);
                })
                .then(() => {
                    var self = this;

                    self.isRecording(true);

                    var proxyObject = {
                        get text() {
                            return self.newPiece.content();
                        },
                        set text(value) {
                            self.newPiece.content(value);
                        }
                    };

                    const stream = WatsonSpeech.SpeechToText.recognizeMicrophone({
                        token: self.token(),
                        continious: true,
                        outputElement: proxyObject,
                        property: 'text'
                    });

                    stream.on('error', error => {
                        console.log(error);
                        self.stopListening();
                    });

                    self.stream(stream);
                });
        }

        stopListening() {
            this.isRecording(false);
            const stream = this.stream();
            if (stream) {
                stream.stop();
            }
            this.stream(null);

            if (this.newPiece.content()) {
                this.conspect.pieces.items.push(this.newPiece.clone());
            }
            this.newPiece.fromDataModel(null);
        }

        removePiece(piece) {
            this.conspect.pieces.items.remove(piece);
        }

        saveConspect() {
            conspectsService.edit(this.conspect)
                .then(() => {
                    location.href = '/';
                });
        }

        playPiece(piece) {
            watsonTokenService.queryText2Speech()
                .then(token => {
                    WatsonSpeech.TextToSpeech.synthesize({
                        token: token,
                        text: piece.content()
                    });
                });
        }
    }

    return RecordPageViewModel;
});