define(function(require) {
    'use strict';

    const baseRunner = require('runners/baseRunner');
    const template = require('text!pages/conspects/conspects.html');
    const PageViewModel = require('pages/conspects/conspects');

    function start() {
        baseRunner.init(template, PageViewModel);
    }

    return {
        start: start
    };
});