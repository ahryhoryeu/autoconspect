define(function(require) {
    'use strict';

    const baseRunner = require('runners/baseRunner');
    const template = require('text!pages/record/record.html');
    const PageViewModel = require('pages/record/record');

    function start() {
        baseRunner.init(template, PageViewModel);
    }

    return {
        start: start
    };
});