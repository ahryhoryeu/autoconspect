define(function (require) {
    'use strict';

    let $ = require('jquery');
    let ko = require('knockout');
    let document = require('global!document');
    let templatesManager = require('helpers/templatesManager');
    let dataProvider = require('helpers/dataProvider');

    let components = require('constants/components');
    let componentsManager = require('helpers/componentsManager');
    let ajaxConfigurator = require('helpers/ajaxConfigurator');

    require('bootstrap-material');
    require('material-ripples');
    require('knockout-sortable');

    require('bindings/date');
    require('bindings/subTreeMutationObserver');
    require('bindings/spinner');

    function init(template, ViewModel) {
        ajaxConfigurator.setUp({
            traditional: true
        });
        ko.bindingHandlers.sortable.strategyMove = true;

        templatesManager.add('page-template', template);
        componentsManager.register(components);

        dataProvider.getData('view-data')
            .then(data => {
                $.material.init();
                $.material.ripples();
                $.material.input();
                ko.applyBindings(new ViewModel(data), document.getElementById('main-view'));
            });
    }

    return {
        init: init
    };
});