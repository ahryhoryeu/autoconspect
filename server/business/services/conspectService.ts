import ConspectRepository from '../../dataAccess/repositories/conspectRepository';
import ConspectShortInfo from '../models/conspectShortInfo';
import DtoConspect from '../models/conspect';
import DtoConspectPiece from '../models/conspectPiece';
import Conspect from '../../dataAccess/models/conspect';
import ConspectPiece from '../../dataAccess/models/conspectPiece';

class ConspectService {
    private readonly conspectRepository: ConspectRepository;

    constructor() {
        this.conspectRepository = new ConspectRepository();
    }

    public async getAllConspects(): Promise<ConspectShortInfo[]> {
        const conspects: Conspect[] = await this.conspectRepository.getAll();

        return conspects.map(c => {
            const dto = new ConspectShortInfo();
            dto.id = c._id.toString();
            dto.name = c.name;

            return dto;
        });
    }

    public async getConspectById(id: string): Promise<DtoConspect> {
        const conspect = await this.conspectRepository.find(id);

        const dto = new DtoConspect();
        dto.id = conspect._id.toString();
        dto.name = conspect.name;
        dto.pieces = conspect.pieces && conspect.pieces.map(p => {
            const piece = new DtoConspectPiece();
            piece.content = p.content;

            return piece;
        });

        return dto;
    }

    public async addConspect(conspect: ConspectShortInfo): Promise<void> {
        const dataConspect = new Conspect();
        dataConspect.name = conspect.name;

        await this.conspectRepository.insert(dataConspect);
    }

    public async deleteConspect(id: string): Promise<void> {
        await this.conspectRepository.delete(id);
    }

    public async updateConspect(conspect: DtoConspect): Promise<void> {
        const dataConspect = new Conspect();
        dataConspect.name = conspect.name;
        dataConspect.pieces = conspect.pieces.map(p => {
            const piece = new ConspectPiece();
            piece.content = p.content;

            return piece;
        });

        await this.conspectRepository.update(dataConspect, conspect.id);
    }
}

export default ConspectService;