import ConspectShortInfo from './conspectShortInfo';
import ConspectPiece from './conspectPiece';

export default class Conspect extends ConspectShortInfo {
    public pieces: ConspectPiece[]; 
};