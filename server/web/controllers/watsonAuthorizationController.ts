import BaseController from './baseController';
import * as watson from 'watson-developer-cloud';
import {speechToTextRequestConfig, textToSpeechRequestConfig} from '../infrastructure/watsonApiConfig';

class WatsonAuthorizationController extends BaseController {
    public getSpeech2TextToken() {
        this.getToken(speechToTextRequestConfig);
    }

    public getText2SpeechToken() {
        this.getToken(textToSpeechRequestConfig);
    }

    private getToken(requestConfig: watson.RequestConfig): void {
        const authService = watson.authorization(requestConfig);

        authService.getToken({ url: requestConfig.url }, (error: string, token: string) => {
            if (error) {
                console.log(`Error while retrieving token: ${error}`);
                this.send('Error retrieving token', 500);

                return;
            }

            this.send(token);
        });
    }
}

export default WatsonAuthorizationController;