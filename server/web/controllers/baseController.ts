import { Request, Response } from 'express';

abstract class BaseController {
    protected request: Request;
    protected response: Response;

    constructor(req: Request, res: Response) {
        this.request = req;
        this.response = res;
    }

    public view(name: string, params?: Object): void {
        this.response.render(name, params);
    }

    public json(object: Object): void {
        this.response.json(object);
    }

    public send(data: any, status = 200): void {
        this.response.status(status).send(data);
    }

    public empty(status = 200): void {
        this.response.status(status).end();
    }
}

export default BaseController;