import BaseController from './baseController';
import ConspectShortInfo from '../../business/models/conspectShortInfo';
import ConspectService from '../../business/services/conspectService';
import Conspect from '../../business/models/conspect';

class ConspectController extends BaseController {
    private readonly conspectService: ConspectService = new ConspectService();

    public async index(): Promise<void> {
        const conspects = await this.conspectService.getAllConspects();
        
        this.view('conspects', {conspects: conspects, title: 'All conspects'});
    }

    public async getAll(): Promise<void> {
        const conspects = await this.conspectService.getAllConspects();

        this.json(conspects);
    }

    public async addConspect(conspect: ConspectShortInfo): Promise<void> {
        await this.conspectService.addConspect(conspect);

        this.empty();
    }

    public async updateConspect(conspect: Conspect): Promise<void> {
        await this.conspectService.updateConspect(conspect);

        this.empty();
    }

    public async deleteConspect(id: string): Promise<void> {
        await this.conspectService.deleteConspect(id);

        this.empty();
    }
}

export default ConspectController;