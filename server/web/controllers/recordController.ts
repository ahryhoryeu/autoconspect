import BaseController from './baseController';
import ConspectService from '../../business/services/conspectService';

class RecordController extends BaseController {
    private readonly conspectService = new ConspectService();

    public async index(conspectId: string): Promise<void> {
        const conspect = await this.conspectService.getConspectById(conspectId);
        
        this.view('record', {
            title: `Record lecture for ${conspect.name}`,
            conspect: conspect
        });
    }
}

export default RecordController;