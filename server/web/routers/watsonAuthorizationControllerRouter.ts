import WatsonAuthorizationController from '../controllers/watsonAuthorizationController';
import { Router } from 'express';

const router = Router();

router.get('/token/speech2text', (request, response) => {
    const controller = new WatsonAuthorizationController(request, response);

    controller.getSpeech2TextToken();
});

router.get('/token/text2speech', (request, response) => {
    const controller = new WatsonAuthorizationController(request, response);

    controller.getText2SpeechToken();
});

export { router as watsonAuthorizationControllerRouter };
