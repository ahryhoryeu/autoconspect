import RecordController from '../controllers/recordController';
import { Router } from 'express';

const router = Router();

router.get('/index', (request, response) => {
    const controller = new RecordController(request, response);
    const conspectId = request.query.conspectId as string;

    controller.index(conspectId);
});

export { router as recordControllerRouter };
