import ConspectController from '../controllers/conspectController';
import ConspectShortInfo from '../../business/models/conspectShortInfo';
import Conspect from '../../business/models/conspect';
import ConspectPiece from '../../business/models/conspectPiece';
import { Router } from 'express';

const router = Router();

router.get('/index', (request, response) => {
    const controller = new ConspectController(request, response);

    controller.index();
});

router.get('/all', (request, response) => {
    const controller = new ConspectController(request, response);

    controller.getAll();
});

router.post('/add', (request, response) => {
    const controller = new ConspectController(request, response);
    if (!request.body) {
        controller.empty(400);
    }

    const info = new ConspectShortInfo();
    info.name = request.body.name;
    controller.addConspect(info);
});

router.post('/edit', (request, response) => {
    const controller = new ConspectController(request, response);
    if (!request.body) {
        controller.empty(400);
    }

    const conspect = new Conspect();
    conspect.id = request.body.id;
    conspect.name = request.body.name;
    conspect.pieces = request.body.pieces && request.body.pieces.map((p: any) => {
        const piece = new ConspectPiece();
        piece.content = p.content;

        return piece;
    });

    controller.updateConspect(conspect);
});

router.post('/remove', (request, response) => {
    const controller = new ConspectController(request, response);
    if (!request.body) {
        controller.empty(400);
    }

    controller.deleteConspect(request.body.id as string);
});

export { router as conspectControllerRouter };
