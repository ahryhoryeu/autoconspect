import * as express from 'express';
import { recordControllerRouter } from './routers/recordControllerRouter';
import { watsonAuthorizationControllerRouter } from './routers/watsonAuthorizationControllerRouter';
import { conspectControllerRouter } from './routers/conspectControllerRouter';
import * as path from 'path';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

const app = express();

app.set('view engine', 'pug');
app.set('views', `${__dirname}/views`);

app.use(logger('dev'));
app.use('/content', express.static(path.join(__dirname, '../../client')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/record', recordControllerRouter);
app.use('/watson', watsonAuthorizationControllerRouter);
app.use('/conspects', conspectControllerRouter);

//default route
app.use((request, response) => response.redirect('/conspects/index'));

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Successfully started server at localhost:${port}`));