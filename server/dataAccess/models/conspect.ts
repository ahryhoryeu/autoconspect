import IEntity from './entity';
import ConspectPiece from './conspectPiece';
import {ObjectID} from 'mongodb';

export default class Concpect implements IEntity {
    public _id: ObjectID;
    public name: string;
    public pieces: ConspectPiece[];
}