import IEntity from './entity';
import {ObjectID} from 'mongodb';

export default class ConspectPiece {
    public date: Date;
    public content: string;
}