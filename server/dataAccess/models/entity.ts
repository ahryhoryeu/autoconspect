import {ObjectID} from 'mongodb';

interface IEntity {
    _id: ObjectID;
}

export default IEntity;