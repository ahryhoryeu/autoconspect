import BaseRepository from './baseRepository';
import Conspect from '../models/conspect'; 

class ConspectRepository extends BaseRepository<Conspect> {
    protected readonly collectionName = 'conspects'; 
}

export default ConspectRepository;