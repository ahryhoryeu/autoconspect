import * as MongoDb from 'mongodb';
import Config from '../config';
import IEntity from '../models/entity';

abstract class BaseRepository<T extends IEntity> {
    private dbConnection: MongoDb.Db | null;
    protected abstract readonly collectionName: string;

    private async connect(): Promise<MongoDb.Db> {
        return MongoDb.MongoClient.connect(Config.url);
    }

    private async getCollection(): Promise<MongoDb.Collection> {
        let connection: MongoDb.Db;
        if (!this.dbConnection) {
            connection = await this.connect();
            this.dbConnection = connection;
        } else {
            connection = this.dbConnection;
        }

        return connection.collection(this.collectionName);
    }

    public async getAll(): Promise<T[]> {
        const collection = await this.getCollection();

        return collection.find({}).toArray();
    }

    public async find(id: string): Promise<T> {
        const collection = await this.getCollection();
        const objectId = new MongoDb.ObjectID(id);

        return collection.findOne({
            _id: objectId
        });
    }

    public async insert(entity: T): Promise<any> {
        const collection = await this.getCollection();
        entity._id = new MongoDb.ObjectID();

        return collection.insertOne(entity);
    }

    public async update(entity: T, id: string): Promise<any> {
        const collection = await this.getCollection();
        entity._id = new MongoDb.ObjectID(id);

        return collection.updateOne(
            {
                _id: entity._id
            }, 
            {
                $set: entity
            });
    }

    public async delete(id: string): Promise<any> {
        const collection = await this.getCollection();
        const objectId = new MongoDb.ObjectID(id);

        return collection.deleteOne({
            _id: objectId
        });
    }

    public async dispose(): Promise<void> {
        if (this.dbConnection) {
            await this.dbConnection.close();
            this.dbConnection = null;
        }
    }
}

export default BaseRepository;